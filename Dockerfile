FROM registry.gitlab.com/sisicmbio/pdci/image_file_dump:empty AS sql
ENV APP_PROJETO "alpine318"
COPY sql  /sql/
ENTRYPOINT ["/sbin/tini", "--","/bin/pdci_upd_sql.sh"]

FROM alpine:3.18 AS develop

LABEL maintainer="Rafael Roque de Mello <roquebrasilia@gmail.com>"
WORKDIR /var/www/html

ENV PACKAGES="supervisor dcron postfix ca-certificates tzdata curl openssl openssh-client libltdl shadow util-linux pciutils   usbutils   coreutils  binutils    findutils  grep  bash bash-completion gettext"

RUN apk add --update --no-cache $PACKAGES && \
     rm -rf /var/lib/apt/lists/* && \
     cp /usr/share/zoneinfo/Brazil/East /etc/localtime && \
     apk del tzdata && \
     #Instalar o teste de mandar erro do shell para o sentry
     curl -sL https://sentry.io/get-cli/ | bash

ADD serpro-ca/root_1.crt /usr/local/share/ca-certificates/root_1.crt
ADD serpro-ca/root_2.crt /usr/local/share/ca-certificates/root_2.crt
ADD serpro-ca/root_3.crt /usr/local/share/ca-certificates/root_3.crt
ADD icmbio-ca/CA_ICMBIO.crt /usr/local/share/ca-certificates/CA_ICMBIO.crt

RUN update-ca-certificates

COPY conf/files/ /

CMD ["/usr/bin/supervisord", "-c", "/etc/supervisord.conf"]


FROM develop AS build

FROM develop AS production
