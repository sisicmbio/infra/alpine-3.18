#!/usr/bin/env bash

set +xe

CI_REGISTRY=registry.gitlab.com

docker logout
docker login ${CI_REGISTRY}

docker pull "${CI_REGISTRY}/sisicmbio/infra/alpine-3.18:develop"

docker run -P "${CI_REGISTRY}/sisicmbio/infra/alpine-3.18:develop"

